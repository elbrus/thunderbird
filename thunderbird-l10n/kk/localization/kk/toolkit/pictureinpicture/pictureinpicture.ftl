# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Суреттегі сурет

pictureinpicture-pause =
    .aria-label = Аялдату
pictureinpicture-play =
    .aria-label = Ойнау

pictureinpicture-mute =
    .aria-label = Дыбысын сөндіру
pictureinpicture-unmute =
    .aria-label = Даусын қосу

pictureinpicture-unpip =
    .aria-label = Қайта бетке жіберу

pictureinpicture-close =
    .aria-label = Жабу

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Аялдату
    .title = Аялдату (бос аралық пернесі)
pictureinpicture-play-cmd =
    .aria-label = Ойнату
    .title = Ойнату (бос аралық пернесі)

pictureinpicture-mute-cmd =
    .aria-label = Дыбысын сөндіру
    .title = Дыбысын сөндіру ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Даусын қосу
    .title = Даусын қосу ({ $shortcut })

pictureinpicture-close-cmd =
    .aria-label = Жабу
    .title = Жабу ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Субтитрлар
    .title = Субтитрлар

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Толық экран режимі
    .title = Толық экран режимі (қос шерту)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Толық экраннан шығу
    .title = Толық экраннан шығу (қос шерту)

pictureinpicture-seekbackward-cmd =
    .aria-label = Артқа
    .title = Артқа (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Алға
    .title = Алға (→)

pictureinpicture-subtitles-label = Субтитрлар

pictureinpicture-font-size-label = Қаріп өлшемі

pictureinpicture-font-size-small = Кішкентай

pictureinpicture-font-size-medium = Орташа

pictureinpicture-font-size-large = Үлкен
