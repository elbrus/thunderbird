# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = Тазарту

## Placeholders for date and time inputs

datetime-year-placeholder = жжжж
datetime-month-placeholder = аа
datetime-day-placeholder = кк
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = Жыл
datetime-month =
    .aria-label = Ай
datetime-day =
    .aria-label = Күн

## Field labels for input type=time

datetime-hour =
    .aria-label = Сағат
datetime-minute =
    .aria-label = Минут
datetime-second =
    .aria-label = Секунд
datetime-millisecond =
    .aria-label = Миллисекунд
datetime-dayperiod =
    .aria-label = AM/PM
