# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Слика у слици

pictureinpicture-pause =
    .aria-label = Пауза
pictureinpicture-play =
    .aria-label = Пуштање

pictureinpicture-mute =
    .aria-label = Искљ. звук
pictureinpicture-unmute =
    .aria-label = Пусти тон

pictureinpicture-unpip =
    .aria-label = Врати на картицу

pictureinpicture-close =
    .aria-label = Затвори

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Пауза
    .title = Пауза (типка за размак)
pictureinpicture-play-cmd =
    .aria-label = Репродукција
    .title = Репродукција (типка за размак)

pictureinpicture-mute-cmd =
    .aria-label = Искључи звук
    .title = Искључи звук ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Укључи звук
    .title = Укључи звук ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Пошаљи назад у језичак
    .title = Врати на језичак

pictureinpicture-close-cmd =
    .aria-label = Затвори
    .title = Затвори ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Титлови
    .title = Титлови

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Цео екран
    .title = Цео екран (дупли клик)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Изађи из целог екрана
    .title = Изађи из целог екрана (дупли клик)

pictureinpicture-seekbackward-cmd =
    .aria-label = Уназад
    .title = Уназад (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Напред
    .title = Напред (→)

pictureinpicture-subtitles-label = Поднаслови

pictureinpicture-font-size-label = Величина фонта

pictureinpicture-font-size-small = Мала

pictureinpicture-font-size-medium = Средња

pictureinpicture-font-size-large = Велика
