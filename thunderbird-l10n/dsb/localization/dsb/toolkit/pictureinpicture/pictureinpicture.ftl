# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Wobraz-we-wobrazu

pictureinpicture-pause =
    .aria-label = Pawza
pictureinpicture-play =
    .aria-label = Wótgraś

pictureinpicture-mute =
    .aria-label = Bźez zuka
pictureinpicture-unmute =
    .aria-label = Ze zukom

pictureinpicture-unpip =
    .aria-label = K rejtarikoju slědk pósłaś

pictureinpicture-close =
    .aria-label = Zacyniś

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Pawza
    .title = Pawza (wobceŕkowa rědka)
pictureinpicture-play-cmd =
    .aria-label = Wótgraś
    .title = Wótgraś (wobceŕkowa rědka)

pictureinpicture-mute-cmd =
    .aria-label = Bźez zuka
    .title = Bźez zuka ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Ze zukom
    .title = Ze zukom ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = K rejtarkoju slědk pósłaś
    .title = Slědk k rejtarikoju

pictureinpicture-close-cmd =
    .aria-label = Zacyniś
    .title = Zacyniś ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Pódtitele
    .title = Pódtitele

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Połna wobrazowka
    .title = Połna wobrazowka (dwójne kliknjenje)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Połnu wobrazowku spušćiś
    .title = Połnu wobrazowku spušćiś (dwójne kliknjenje)

pictureinpicture-seekbackward-cmd =
    .aria-label = Slědk
    .title = Slědk (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Doprědka
    .title = Doprědka (→)

pictureinpicture-subtitles-label = Pódtitele

pictureinpicture-font-size-label = Pismowa wjelikosć

pictureinpicture-font-size-small = Mały

pictureinpicture-font-size-medium = Srjejźny

pictureinpicture-font-size-large = Wjeliki
