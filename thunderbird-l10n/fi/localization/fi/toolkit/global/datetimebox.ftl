# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = Tyhjennä

## Placeholders for date and time inputs

datetime-year-placeholder = vvvv
datetime-month-placeholder = kk
datetime-day-placeholder = pp
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = Vuosi
datetime-month =
    .aria-label = Kuukausi
datetime-day =
    .aria-label = Päivä

## Field labels for input type=time

datetime-hour =
    .aria-label = Tunnit
datetime-minute =
    .aria-label = Minuutit
datetime-second =
    .aria-label = Sekunnit
datetime-millisecond =
    .aria-label = Millisekunnit
datetime-dayperiod =
    .aria-label = Aamupäivä/iltapäivä
