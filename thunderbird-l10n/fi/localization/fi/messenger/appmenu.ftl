# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## New Account

appmenu-new-account-panel-title =
    .title = Uusi tili

appmenu-new-account-panel =
    .label = Uusi tili
    .accesskey = U

appmenu-create-new-mail-account =
    .label = Hanki uusi sähköpostiosoite
    .accesskey = H

appmenu-new-mail-account =
    .label = Olemassa oleva sähköpostiosoite
    .accesskey = E

appmenu-new-calendar =
    .label = Kalenteri
    .accesskey = K

appmenu-new-chat-account =
    .label = Keskustelu
    .accesskey = e

appmenu-new-feed =
    .label = Syöte
    .accesskey = ö

appmenu-new-newsgroup =
    .label = Keskusteluryhmä
    .accesskey = r

## New Account / Address Book

appmenu-newab-panel-title =
    .title = Uusi osoitekirja

appmenu-newab-panel =
    .label = Uusi osoitekirja
    .accesskey = A

appmenu-new-addressbook =
    .label = Paikallinen osoitekirja
    .accesskey = A

appmenu-new-carddav =
    .label = CardDav-osoitekirja
    .accesskey = C

appmenu-new-ldap =
    .label = LDAP-osoitekirja
    .accesskey = L

## Create

appmenu-create-panel-title =
    .title = Luo

appmenu-create-panel =
    .label = Luo
    .accesskey = L

appmenu-create-message =
    .label = Viesti
    .accesskey = V

appmenu-create-event =
    .label = Tapahtuma
    .accesskey = T

appmenu-create-task =
    .label = Tehtävä
    .accesskey = ä

appmenu-create-contact =
    .label = Yhteystieto
    .accesskey = Y

## Open

appmenu-open-panel =
    .label = Avaa
    .accesskey = A

appmenu-open-panel-title =
    .title = Avaa

appmenu-open-file-panel =
    .label = Avaa tiedostosta
    .accesskey = v

appmenu-open-file-panel-title =
    .title = Avaa tiedostosta

appmenu-open-message =
    .label = Viesti…
    .accesskey = V

appmenu-open-calendar =
    .label = Kalenteri…
    .accesskey = K

## View / Layout

appmenu-view-panel-title =
    .title = Näytä

appmenu-view-panel =
    .label = Näytä
    .accesskey = N

appmenu-font-size-value = Fontin koko

appmenu-mail-uidensity-value = Tiiviys

appmenu-uidensity-compact =
    .tooltiptext = Tiivis

appmenu-uidensity-default =
    .tooltiptext = Oletus

appmenuitem-font-size-enlarge =
    .tooltiptext = Suurenna fontin kokoa

appmenuitem-font-size-reduce =
    .tooltiptext = Pienennä fontin kokoa

## Tools

appmenu-tools-panel-title =
    .title = Työkalut

appmenu-tools-panel =
    .label = Työkalut
    .accesskey = T

appmenu-tools-import =
    .label = Tuonti
    .accesskey = u

appmenu-tools-export =
    .label = Vienti
    .accesskey = V

appmenu-tools-message-search =
    .label = Etsi viesteistä
    .accesskey = s

appmenu-tools-dev-tools =
    .label = Kehittäjätyökalut
    .accesskey = ä

## Help

appmenu-help-panel-title =
    .title = Ohje

appmenu-help-get-help =
    .label = Tuki
    .accesskey = T

appmenu-help-explore-features =
    .label = Tutustu ominaisuuksiin
    .accesskey = F

appmenu-help-shortcuts =
    .label = Pikanäppäimet
    .accesskey = k

appmenu-help-get-involved =
    .label = Tule mukaan
    .accesskey = m

appmenu-help-donation =
    .label = Tee lahjoitus
    .accesskey = h

appmenu-help-share-feedback =
    .label = Jaa ideoita ja palautetta
    .accesskey = d

appmenu-help-about-product =
    .label = Tietoja: { -brand-short-name }
    .accesskey = A
