# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Aslaɣ n uvidyu

pictureinpicture-pause =
    .aria-label = Seṛǧu
pictureinpicture-play =
    .aria-label = Urar

pictureinpicture-mute =
    .aria-label = Asusam
pictureinpicture-unmute =
    .aria-label = Amesli

pictureinpicture-unpip =
    .aria-label = Ales tuzna deg yiccer

pictureinpicture-close =
    .aria-label = Mdel

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Asteɛfu
    .title = Asteɛfu (Afeggag n tallunt)
pictureinpicture-play-cmd =
    .aria-label = Urar
    .title = Urar (Afeggag n tallunt)

pictureinpicture-mute-cmd =
    .aria-label = Sgugem
    .title = Sgugem ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Sgugem
    .title = Sgugem ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Ales tuɣalin ɣer yiccer
    .title = Tuɣalin ɣer yiccer

pictureinpicture-close-cmd =
    .aria-label = Mdel
    .title = Mdel ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Iduzwilen
    .title = Iduzwilen

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Agdil ačuran
    .title = Agdil ačuran (sin isitiyen)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Ffeɣ seg ugdil ačuran
    .title = Ffeɣ seg ugdil ačuran (sin isitiyen)

pictureinpicture-seekbackward-cmd =
    .aria-label = Tuɣalin ɣer deffir
    .title = Tuɣalin ɣer deffir (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Welleh
    .title = Welleh (→)

pictureinpicture-subtitles-label = Iduzwilen

pictureinpicture-font-size-label = Teɣzi n tsefsit

pictureinpicture-font-size-small = Mecṭuḥ

pictureinpicture-font-size-medium = Alemmas

pictureinpicture-font-size-large = Hraw
