# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = תמונה בתוך תמונה

pictureinpicture-pause =
    .aria-label = השהיה
pictureinpicture-play =
    .aria-label = ניגון

pictureinpicture-mute =
    .aria-label = השתקה
pictureinpicture-unmute =
    .aria-label = ביטול השתקה

pictureinpicture-unpip =
    .aria-label = חזרה ללשונית

pictureinpicture-close =
    .aria-label = סגירה

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = השהייה
    .title = השהייה (מקש הרווח)
pictureinpicture-play-cmd =
    .aria-label = ניגון
    .title = ניגון (מקש הרווח)

pictureinpicture-mute-cmd =
    .aria-label = השתקה
    .title = השתקה ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = ביטול השתקה
    .title = ביטול השתקה ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = חזרה ללשונית
    .title = חזרה ללשונית

pictureinpicture-close-cmd =
    .aria-label = סגירה
    .title = סגירה ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = כתוביות
    .title = כתוביות

##

pictureinpicture-fullscreen-cmd =
    .aria-label = מסך מלא
    .title = מסך מלא (לחיצה כפולה)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = יציאה ממסך מלא
    .title = יציאה ממסך מלא (לחיצה כפולה)

pictureinpicture-seekbackward-cmd =
    .aria-label = אחורה
    .title = אחורה (←)

pictureinpicture-seekforward-cmd =
    .aria-label = קדימה
    .title = קדימה (→)

pictureinpicture-subtitles-label = כתוביות

pictureinpicture-font-size-label = גודל גופן

pictureinpicture-font-size-small = קטן

pictureinpicture-font-size-medium = בינוני

pictureinpicture-font-size-large = גדול
