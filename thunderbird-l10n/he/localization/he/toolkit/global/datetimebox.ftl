# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = ניקוי

## Placeholders for date and time inputs

datetime-year-placeholder = שנה
datetime-month-placeholder = חודש
datetime-day-placeholder = יום
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = שנה
datetime-month =
    .aria-label = חודש
datetime-day =
    .aria-label = יום

## Field labels for input type=time

datetime-hour =
    .aria-label = שעות
datetime-minute =
    .aria-label = דקות
datetime-second =
    .aria-label = שניות
datetime-millisecond =
    .aria-label = מילישניות
datetime-dayperiod =
    .aria-label = AM/PM
