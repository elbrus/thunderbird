# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Picture-in-Picture

pictureinpicture-pause =
    .aria-label = Pauze
pictureinpicture-play =
    .aria-label = Spylje

pictureinpicture-mute =
    .aria-label = Lûd út
pictureinpicture-unmute =
    .aria-label = Lûd oan

pictureinpicture-unpip =
    .aria-label = Weromstjoere nei ljepblêd

pictureinpicture-close =
    .aria-label = Slute

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Pauzearje
    .title = Pauzearje (Spaasjebalke)
pictureinpicture-play-cmd =
    .aria-label = Ofspylje
    .title = Ofspylje (Spaasjebalke)

pictureinpicture-mute-cmd =
    .aria-label = Lûd út
    .title = Lûd út ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Lûd oan
    .title = Lûd oan ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Weromstjoere nei ljepblêd
    .title = Werom nei ljepblêd

pictureinpicture-close-cmd =
    .aria-label = Slute
    .title = Slute ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Undertitels
    .title = Undertitels

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Folslein skerm
    .title = Folslein skerm (dûbelklik)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Folslein skerm ferlitte
    .title = Folslein skerm ferlitte (dûbelklik)

pictureinpicture-seekbackward-cmd =
    .aria-label = Tebek
    .title = Tebek (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Foarút
    .title = Foarút (→)

pictureinpicture-subtitles-label = Undertitels

pictureinpicture-font-size-label = Lettergrutte

pictureinpicture-font-size-small = Lyts

pictureinpicture-font-size-medium = Gemiddeld

pictureinpicture-font-size-large = Grut
